import { Component, OnInit } from '@angular/core';
import { Client } from '../model/client';
import { ClientService } from '../services/client.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-modifier-client',
  templateUrl: './modifier-client.component.html',
  styleUrls: ['./modifier-client.component.css']
})
export class ModifierClientComponent implements OnInit {
  id!:number;
  client:Client;
  submitted=false;
  isSuccessfulUpdate=false;
  constructor(private clientService:ClientService, private router:Router,
              private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.client=new Client();
    this.id=this.route.snapshot.params.id;
    this.clientService.client(this.id).subscribe(data=>{
      this.client=data;
    },
    error=>{
      console.log(error)
    })

  }

  goToList(){
    this.router.navigateByUrl('clients')

   }
  modifierClient(){
    this.submitted=true;
    this.clientService.modifierClient(this.client, this.id).subscribe(data=>{
      this.isSuccessfulUpdate=true;
      this.client=new Client();
      this.router.navigate(['clients']);
    },
    error => {
      console.log(error)
    });
    console.log("okk")
  }

}
