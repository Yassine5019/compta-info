import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RegisterComponent } from './register/register.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HttpClientXsrfModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { JwtHelperService, JWT_OPTIONS  } from '@auth0/angular-jwt';
import { LoginComponent } from './login/login.component';
import { AuthInterceptor } from './services/auth-interceptor';
import { CreerClientComponent } from './creer-client/creer-client.component';
import { ModifierClientComponent } from './modifier-client/modifier-client.component';
import { DeleteClientComponent } from './delete-client/delete-client.component';
import { ListeClientComponent } from './liste-client/liste-client.component';
import { DetailsClientComponent } from './details-client/details-client.component';


@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    LoginComponent,
    CreerClientComponent,
    ModifierClientComponent,
    DeleteClientComponent,
    ListeClientComponent,
    DetailsClientComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    BrowserAnimationsModule,
    HttpClientModule,
    HttpClientXsrfModule.withOptions({cookieName:'XSRF-TOKEN'})
  ],
  providers: [
              JwtHelperService,
              { provide: JWT_OPTIONS, useValue: JWT_OPTIONS },
              { provide: HTTP_INTERCEPTORS, useClass:AuthInterceptor, multi:true}
             ],
  bootstrap: [AppComponent]
})
export class AppModule { }
