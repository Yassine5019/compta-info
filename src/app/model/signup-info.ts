export class SignupInfo{
    name:string;
    username:string;
    email:string;
    role:string[];
    password:string;

    constructor(name: string, username: string, email: string, password: string){
      this.name=name;
      this.username=username;
      this.role=['ROLE_USER'];
      this.email=email;
      this.password=password;
    }
}