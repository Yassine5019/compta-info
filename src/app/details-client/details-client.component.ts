import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Client } from '../model/client';
import { ClientService } from '../services/client.service';

@Component({
  selector: 'app-details-client',
  templateUrl: './details-client.component.html',
  styleUrls: ['./details-client.component.css']
})
export class DetailsClientComponent implements OnInit {
  id!:number
  client!:Client
  constructor(private router:Router, private route:ActivatedRoute, private clientService:ClientService) { }

  ngOnInit(): void {
    this.client=new Client();
    this.id=this.route.snapshot.params.id;
    this.clientService.client(this.id).subscribe(data=>{
      this.client=data;
    },
    error=>{
      console.log(error)
    })
  }

  goToList(){
   this.router.navigateByUrl('clients');
  }

}
