import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { AuthGuardService as AuthGuard } from './services/auth-guard.service';
import { ListeClientComponent } from './liste-client/liste-client.component';
import { DetailsClientComponent } from './details-client/details-client.component';
import { CreerClientComponent } from './creer-client/creer-client.component';
import { ModifierClientComponent } from './modifier-client/modifier-client.component';

const routes: Routes = [
  {
    path : 'clients',
    component: ListeClientComponent
  },
  {
    path : 'creer-client',
    component: CreerClientComponent
  },
  {
    path : 'modifier-client/:id',
    component: ModifierClientComponent
  },
  {
    path : 'details-client/:id',
    component: DetailsClientComponent
  },
  {
    path :'register',
    component : RegisterComponent
  },
  {
    path :'login',
    component : LoginComponent
  },
  {
    path :'',

    redirectTo : '/login',

    pathMatch : 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
