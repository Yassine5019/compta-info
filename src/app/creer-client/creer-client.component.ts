import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Client } from '../model/client';
import { ClientService } from '../services/client.service';

@Component({
  selector: 'app-creer-client',
  templateUrl: './creer-client.component.html',
  styleUrls: ['./creer-client.component.css']
})
export class CreerClientComponent implements OnInit {
  client:Client= new Client();
  isSuccessfulSave=false;
  submitted=false;
  constructor(private clientService:ClientService, private router:Router ) { }

  ngOnInit(): void {
  }

  creerClient(){
    this.submitted=true;
    this.clientService.creerClient(this.client)
    .subscribe(data => {
      this.isSuccessfulSave=true;
      this.client=new Client();
      this.router.navigate(['clients']);

      console.log("okkkkkkkkk")
    }, error => {
      console.log(error)
    });
  }

  goToList(){
    this.router.navigateByUrl('clients');
  }
}
