import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable, BehaviorSubject} from 'rxjs';

import { JwtHelperService} from '@auth0/angular-jwt';
import { map } from 'rxjs/operators';
import { Router} from '@angular/router';
import { TokenStorageService } from '../services/token-storage.service';
import { SignupInfo } from '../model/signup-info';
import { JwtResponse } from '../model/jwt-response';
import { AuthLoginInfo } from '../model/auth-login-info';

const httpOptions = {
 headers: new HttpHeaders({ 'Content-Type' : 'application/json' })
}

const TOKEN_KEY='AuthToken';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
 private currentUserSubject! : BehaviorSubject<any>;
 public currentUser! : Observable<any>;
 private signupUrl='http://localhost:8090/api/auth/register'
 private loginUrl='http://localhost:8090/api/auth/login'

 constructor(private http:HttpClient, private jwtHelper:JwtHelperService,
             private tokenStorage:TokenStorageService, private router:Router) {
   this.currentUserSubject=new BehaviorSubject<any>(sessionStorage.getItem(TOKEN_KEY));
   this.currentUser=this.currentUserSubject.asObservable();
 }

 public get currentUserValue() : any{
   return this.currentUserSubject.value;
 }

 signUp(signUpInfo:SignupInfo){
   return this.http.post<JwtResponse>(this.signupUrl, signUpInfo, httpOptions)
   .pipe(map(data=>{
     this.saveUserData(data);
     return data
   }))
 }

 login(authLoginInfo:AuthLoginInfo){
   return this.http.post<JwtResponse>(this.loginUrl, authLoginInfo, httpOptions)
   .pipe(map(data=>{
    this.saveUserData(data);
    return data
  }))
 }

 private saveUserData(data : any){
   this.tokenStorage.saveToken(data.accessToken)
   this.tokenStorage.saveUsername(data.username)
   this.tokenStorage.saveAuthorities(data.authorities)
   this.currentUserSubject.next(data.accessToken)
 }

 isAuthenticated() {
  return this.http.get('/register/login');
}


}
