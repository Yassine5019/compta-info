import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Client } from '../model/client';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class ClientService {

 private baseUrl='http://localhost:8090/api/clients'
  constructor(private httpService:HttpClient) { }

  creerClient(client : Client) : Observable<any>{
     //return this.httpService.post(this.baseUrl + '/saveClient', client);
     return this.httpService.post(`${this.baseUrl}/saveClient`, client);
  }

  modifierClient(client : Client, id:any) : Observable<any>{
    //return this.httpService.post(this.baseUrl + '/saveClient', client);
    return this.httpService.put(`${this.baseUrl}/updateClient/${id}`, client);
  }

  supprimerClient(id:any) : Observable<any>{
    //return this.httpService.post(this.baseUrl + '/saveClient', client);
    return this.httpService.delete(`${this.baseUrl}/deleteClient/${id}`);
  }

  clients() : Observable<Client[]>{
    //return this.httpService.post(this.baseUrl + '/saveClient', client);
    return this.httpService.get<Client[]>(`${this.baseUrl}/listeClients`);
  }
//   .pipe(map(myAnswers => {
//     const myTransformedAnswer : any = [];

//     myAnswers.forEach((item) => {
//         myTransformedAnswer.push({
//             id: item.id,
//             title: item.codeClient,
//             description: item.dateCloture
//         });
//     });

//     return myTransformedAnswer;
// }));

  client(id:any) : Observable<any>{
    //return this.httpService.post(this.baseUrl + '/saveClient', client);
    return this.httpService.get(`${this.baseUrl}/client/${id}`);
  }

}
