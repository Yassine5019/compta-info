import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { AuthService } from '../services/auth.service';
import { Router} from '@angular/router';
import { BehaviorSubject} from 'rxjs';
import { TokenStorageService } from '../services/token-storage.service';
import { AuthLoginInfo } from '../model/auth-login-info';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  form:any={};
  errorMessage='';
  isLoginFailed=false;
  isLoginSuccessful=false;

  constructor(private authService:AuthService, private router: Router,
              private tokenStorageService: TokenStorageService, private activatedRoute:ActivatedRoute) { }

  ngOnInit(): void {
  }
  onSubmit(){
    this.authService.login(new AuthLoginInfo(this.form.username,this.form.password))
    .pipe(first()).subscribe(
      data => {
        this.isLoginSuccessful=true;
        this.isLoginFailed=false;
        this.router.navigate(['clients']);
      },
      errors =>{
        this.errorMessage=errors.error.message;
        this.isLoginFailed=true;
      }
    )
  }

  inscrire(){
    this.router.navigate(['register'])
  }
}
