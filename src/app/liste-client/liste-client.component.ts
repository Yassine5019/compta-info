import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ClientService } from '../services/client.service';
import { Client } from '../model/client';

@Component({
  selector: 'app-liste-client',
  templateUrl: './liste-client.component.html',
  styleUrls: ['./liste-client.component.css']
})
export class ListeClientComponent implements OnInit {
  clients?:Client[];
  clientVal:FormControl;

  constructor(private router : Router, private clientService:ClientService) { }

  ngOnInit(): void {
    this.clientVal=new FormControl()
    this.onReloadData();
  }

  onReloadData(){
    this.clientService.clients().subscribe(data=>{
      this.clients=data
    }, error =>{
      console.log(error)
    })
  }

  supprimerClient(id:number){
    this.clientService.supprimerClient(id).subscribe(data=>{
      this.onReloadData
    }, error =>{
      console.log(error)
    })
  }

  modifierClient(id:number){
    this.router.navigate(['modifier-client', id])
  }

  detailsClient(id:number){
    this.router.navigate(['details-client', id])
  }

}
