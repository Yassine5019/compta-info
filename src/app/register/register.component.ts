import { Component, OnInit } from '@angular/core';
import { SignupInfo } from '../model/signup-info';
import { Router} from '@angular/router';
import { first } from 'rxjs/operators';
import { AuthService } from '../services/auth.service';



@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
form:any={};
signupInfo:any=SignupInfo;
isSuccessful = false;
isSignUpFailed=false;
errorMessage='';
  constructor(private authService:AuthService, private router:Router) { }

  ngOnInit(): void {
  }

  onSubmit(){
    this.signupInfo=new SignupInfo(this.form.name, this.form.username, this.form.email, this.form.password);
    this.authService.signUp(this.signupInfo).pipe(first()).subscribe(
      data => {
        this.isSuccessful=true;
        this.isSignUpFailed=false;
        this.router.navigate(['login']);
      },
      errors =>{
        this.errorMessage=errors.error.message;
        this.isSignUpFailed=true;
      }
    )
  }

  authentifier(){
    this.router.navigate(['login'])
  }

}
